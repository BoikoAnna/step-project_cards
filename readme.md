

# During the development of the project, the following technologies were developed:
* HTML using semantic tags;
* NPM and its modules;
* AJAX requests (fetch);
* ES6 modules;
* Used class in code;
___

# The composition of the project developers:
* ___Anna Boiko___
* ___Mykola Borysenko___
* ___Viktor Hannochenko___
___

# List of completed works:
* __Anna Boiko__: 
   * Made the whole filter scripts
   * Made filter bar in SCSS
   * Set up a project architecture (files and directories)
   * Made cards in scss
   * Made login modal & visit modal in Scss
* __Mykola Borysenko__:
   * Made login modal & visit modal classes & scripts
   * Made login modal & visit modal in Scss and HTML
   * Made a basic HTML and SCSS skeleton of the project
   * Set up a project architecture (files and directories)
* __Viktor Hannochenko__:
   * Made doctors' scripts
   * Made visit class & scripts
   * Made logic in the cards
   * Made cards in HTML
   * Worked on getting token logic via login modal and its navigation through the project
   * Made loading of all cards logic after authorization
___

___
# To start the project, use the following commands:
   1. npm i
   2. gulp build
   3. gulp dev