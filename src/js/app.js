import * as flsFunctions from "./modules/functions.js";
import LoginModal from "./modules/modalSubClass/LoginModal.js";
import VisitModal from "./modules/modalSubClass/VisitModal.js";
import * as filterVisits from "./modules/modalSubClass/filterCards.js";

// import { getToken } from './modules/modalSubClass/LoginModal.js';
// import { dataService } from "./modules/services.js";
// import { noVisitsInfo } from './modules/main.js';
// import { VisitView, loadVisits } from "./modules/visitController.js";

flsFunctions.isWebp();

//Функція для запуску початкової логіки на сторінці
function initialFunction() {
   const loginModal = new LoginModal('#modal-login', '#btn-open-modal-login');
   const visitModal = new VisitModal('#modal-visit', '#btn-open-modal-visit');

   // отриманя токена з localStorage
   // const token = getToken();
   // const token = localStorage.getItem('userToken');

   // if (token) {
   //    // ініціалізація візитів
   //    // (console.log('loadVisits'));
   //    // loadVisits();
   //    //створювати класс треба по за класом - бо викликати метод можна в екземпляру
   //    const visitsView = new VisitView();
   //    visitsView.getAllVisits();


   // } else {
   //    // показати 'No items have been added'
   //    noVisitsInfo();
   //    // contentVisits.innerHTML = `<div class="no-visits"><span>No items have been added</span></div>`;
   // }
}
initialFunction()