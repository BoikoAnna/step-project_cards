import { contentVisits } from './visitController.js';

// інформація для div.content__cards, коли у нас немає карток або ми не авторизувалися

export function noVisitsInfo() {
   contentVisits.innerHTML = '<div class="no-visits"><span>No items have been added</span></div>';
}
