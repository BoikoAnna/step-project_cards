import Modal from "../Modal.js";
import { dataService } from "../services.js";
import { VisitView } from "../visitController.js";
import { noVisitsInfo } from '../main.js';
noVisitsInfo();
export default class LoginModal extends Modal {
   constructor(modalSelector, triggerBtnSelector) {
      super(modalSelector, triggerBtnSelector);
      this.loginForm = this.modal.querySelector('.login-form');
      this.loginFormSubmitHandler = this.loginFormSubmitHandler.bind(this);
      this.loginForm.addEventListener('submit', this.loginFormSubmitHandler);
   }

   // перевірка логіну та пароля
   //Your token is: a79cdc49-265c-4419-9997-900e9ce4c5dc
   //Authorization: `Bearer ${token}`


   async loginFormSubmitHandler(e) {
      e.preventDefault();
      const email = this.loginForm.querySelector('#email').value;
      const password = this.loginForm.querySelector('#password').value;

      try {
         const loginResponse = await fetch('https://ajax.test-danit.com/api/v2/cards/login', {
            method: 'POST',
            headers: {
               'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email, password })
         });
         const userToken = await loginResponse.text();

         if (!loginResponse.ok) {
            throw new Error(userToken);
         }

         // записую токен юзера в локалСтор, для доступу з інших класів
         localStorage.setItem('userToken', userToken);
         console.log(userToken);

         //newDataService не виведе в консоль - асинхронний метод - треба робити вивід консоль в ньому
         // const newDataService = dataService.allVisits;
         // console.log(newDataService);

         //виклик консолі в allVisits
         // dataService.allVisits;
               const visitsView = new VisitView();
               visitsView.getAllVisits();

         // console.log(dataService.allVisits.then(data => data));
         // console.log(dataService.allVisits);

         // Тут повинен бути виклик методу, який показує список візитів
         // Visit(s).showVisitsList();
         // new VisitView().getAllVisits(); невдала спроба

         //зміна кнопки на logout
         this.logout();

         this.close();
      } catch (error) {
         console.error(error);
      }
      // getToken.localStorage.getItem()
   }

   logout() {
      const logoutBtn = document.querySelector('#btn-logout');
      const loginBtn = document.querySelector('#btn-open-modal-login');

      loginBtn.classList.add('login-hide');
      logoutBtn.classList.remove('logout-hide');

      const logoutClick = function () {
         if (confirm('Are you sure you want to log out?')) {
            //для виходу - видаляю токен з локал стор
            localStorage.removeItem('userToken');
            logoutBtn.classList.add('logout-hide');
            loginBtn.classList.remove('login-hide');
            logoutBtn.removeEventListener('click', logoutClick);
            noVisitsInfo();
         }
      }

      logoutBtn.addEventListener('click', logoutClick)
   }
}
//навіщо тут getToken()?
// export function getToken() {
//    return localStorage.getItem('userToken');
// }

//  export function getToken() {
//    return localStorage.getItem('token') ? localStorage.getItem('token') : null;
//  }