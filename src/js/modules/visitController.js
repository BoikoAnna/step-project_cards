import {
   dataService
} from './services.js';
import {
   noVisitsInfo
} from './main.js';
import {
   VisitDentist,
   VisitDentistTemplate
} from './visitDentist.js';
import {
   VisitCardiologist,
   VisitCardiologistTemplate,
} from './visitCardiologist.js';
import {
   VisitTherapist,
   VisitTherapistTemplate
} from './visitTherapist.js';

export const contentVisitsCss = 'content__visits';
export const parentVisitCss = 'visit';
export const parentVisitShadowCss = 'shadow';
export const contentVisits = document.querySelector(`.${contentVisitsCss}`);

// controller load/view visit
export class VisitView {

   constructor() {
      this.element = document.querySelector('.content__visits');
      this.dataService = dataService;
   }

   setVisit(visit) {
      let visitElement = null;
      return this.dataService
         .createVisit(visit)
         .then((response) => response.json())
         .then((visit) => {
            if (visit.doctor.toLowerCase() === 'dentist') {
               visitElement = new VisitDentistTemplate(visit);
            } else if (visit.doctor.toLowerCase() === 'cardiologist') {
               visitElement = new VisitCardiologistTemplate(visit);
            } else if (visit.doctor.toLowerCase() === 'therapist') {
               visitElement = new VisitTherapistTemplate(visit);
            } else {
               throw new Error(
                  'Error parse visit (setVisit() in visitController.js)'
               );
            }
            if (visitElement) {
               if (document.querySelector(`.${contentVisitsCss} .no-visits`)) {
                  this.element.innerHTML = '';
               }
               visitElement.render(this.element);
            }
         })
         .catch((error) => console.log(error))
         .finally(() => {});
   }

   editVisit(visit) {
      let visitElement = null;
      return this.dataService
         .updateVisit(visit.id, visit)
         .then((response) => response.json())
         .then((visit) => {
            if (visit.doctor.toLowerCase() === 'dentist') {
               visitElement = new VisitDentistTemplate(visit);
            } else if (visit.doctor.toLowerCase() === 'cardiologist') {
               visitElement = new VisitCardiologistTemplate(visit);
            } else if (visit.doctor.toLowerCase() === 'therapist') {
               visitElement = new VisitTherapistTemplate(visit);
            } else {
               throw new Error(
                  'Error parse visit (setVisit() in visitController.js)'
               );
            }
            if (visitElement) {
               const deleteOldVisit = document.querySelector(
                  `[data-visit-id="${visit.id}"]`
               );
               deleteOldVisit.remove();
               visitElement.render(this.element);
            }
         })
         .catch((error) => console.log(error))
         .finally(() => {});
   }
changeObjectArray(visits) {
   console.log(visits);
const visitElements = []; 
visits.forEach((visit) => {
   // console.log(visit.doctor);
   if (visit.doctor.toLowerCase() === 'dentist') {
      visitElements.push(new VisitDentistTemplate(visit));
   } else if (visit.doctor.toLowerCase() === 'cardiologist') {
      visitElements.push(new VisitCardiologistTemplate(visit));
   } else if (visit.doctor.toLowerCase() === 'therapist') {
      visitElements.push(new VisitTherapistTemplate(visit));}
   // } else
   //    throw new Error(
   //       'Error parse visit (getAllVisits() in visitController.js)'
   //    );

});
return visitElements;

}
   getAllVisits() {
 
      this.dataService.allVisits
         .then((visits) => {
            if (visits.length === 0) {
               // noVisitsInfo();
               contentVisits.innerHTML = '<div class="no-visits"><span>No items have been added</span></div>'
               return;
            }
            
            this.insertAllVisits(this.changeObjectArray(visits));
         })
         .catch((error) => console.log(error))
         .finally(() => {});
   }

   insertAllVisits(visitElements) {
      console.log (visitElements);
      this.element.innerHTML = '';
      visitElements.forEach((visitElement) => {
         visitElement.render(this.element);
      });
   }
}

// зміна лікаря
export function getDoctor(doctor) {
   let result = null;
   switch (doctor) {
      case 'cardiologist':
         result = 'Кардиолог';
         break;
      case 'dentist':
         result = 'Стоматолог';
         break;
      case 'therapist':
         result = 'Терапевт';
         break;
   }
   return result;
}

// зміна терміновості
export function getUrgency(status) {
   let result = null;
   switch (status) {
      case 'emergency':
         result = 'Невідкладна';
         break;
      case 'priority':
         result = 'Пріорітетна';
         break;
      case 'normal':
         result = 'Звичайна';
         break;
   }
   return result;
}

// зміна виконання візиту
export function getCompleted(status) {
   let result = null;
   switch (status) {
      case 'open':
         result = 'візит запланований';
         break;
      case 'done':
         result = 'визит пройшов';
         break;
   }
   return result;
}

// event for open more-info/delete/edit visit
export function visitHandler(e) {
   const parent = e.target.closest(`.${parentVisitCss}`);
   console.log(parentVisitCss);
   if (e.target.classList.contains('btn-more')) {
      const moreContent = document.querySelector(
         `[data-visit-id="${parent.dataset.visitId}"] .visit__more-info`
      );
      moreContent.classList.toggle('show');
      const showLess = document.querySelector(
         `[data-visit-id="${parent.dataset.visitId}"] .btn-less`
      );
      showLess.classList.toggle('show');

      const showMore = document.querySelector(
         `[data-visit-id="${parent.dataset.visitId}"] .btn-more`
      );
      showMore.classList.toggle('show');

   }


   if (e.target.classList.contains('btn-less')) {
      const lessContent = document.querySelector(
         `[data-visit-id="${parent.dataset.visitId}"] .visit__more-info`
      );
      lessContent.classList.toggle('show');
      const showLess = document.querySelector(
         `[data-visit-id="${parent.dataset.visitId}"] .btn-less`
      );
      showLess.classList.toggle('show');
      const showMore = document.querySelector(
         `[data-visit-id="${parent.dataset.visitId}"] .btn-more`
      );
      showMore.classList.toggle('show');

   }

   // if (e.target.classList.contains('btn-edit')) {
   //    const editId = parent.dataset.visitId;
   //    const btn = document.querySelector(
   //       `[data-visit-id="${parent.dataset.visitId}"] .btn-edit`
   //    );
   //    // dataService
   //    //   .getVisit(editId)
   //    //   .then((data) => loadModal(data));
   // }

   if (e.target.classList.contains('btn-delete')) {
      const deleteId = parent.dataset.visitId;
      dataService
         .deleteVisit(deleteId)
         .then((response) => {
            if (response.ok) {
               parent.remove();
            } else {
               throw new Error('Erorr delete visit');
            }
         })
         .then(() => {
            if (!contentVisits.hasChildNodes()) {
               noVisitsInfo();
            }
         })
         .catch((error) => console.log(error));
   }
}

// створення картки
export function loadVisits() {
   // отримати всі візити
   const visitsView = new VisitView(contentVisits);
   visitsView.getAllVisits();
}

// insert new visit
export async function insertNewVisit(visit) {
   const doctorObj = createDoctorObj(visit);
   const visitView = await new VisitView(contentVisits);
   await visitView.setVisit(doctorObj);
}
// вставити новий візит
export async function insertEditVisit(visit) {
   const doctorObj = createDoctorObj(visit);
   const visitView = await new VisitView(contentVisits);
   await visitView.editVisit(doctorObj);
}

// створити об'єкт visit "Doctor"
function createDoctorObj(visit) {
   let doctorObj = {};
   switch (visit.doctor) {
      case 'cardiologist':
         doctorObj = new VisitCardiologist(
            visit.id,
            visit.name,
            visit.doctor,
            visit.purpose,
            visit.description,
            visit.urgency,
            visit.completed,
            visit.pressure,
            visit.bmi,
            visit.heartDisease,
            visit.age
         );
         break;
      case 'dentist':
         doctorObj = new VisitDentist(
            visit.id,
            visit.name,
            visit.doctor,
            visit.purpose,
            visit.description,
            visit.urgency,
            visit.completed,
            visit.lastVisit
         );
         break;
      case 'therapist':
         doctorObj = new VisitTherapist(
            visit.id,
            visit.name,
            visit.doctor,
            visit.purpose,
            visit.description,
            visit.urgency,
            visit.completed,
            visit.age
         );
         break;
      default:
         return;
   }
   return doctorObj;
}