import {
  Visit
} from "./Visit.js";
import {
  visitHandler,
  getDoctor,
  getUrgency,
  getCompleted,
  parentVisitCss,
  parentVisitShadowCss,
} from "./visitController.js";

export class VisitTherapist extends Visit {
  constructor(
    visitId,
    name,
    doctor,
    purpose,
    description,
    urgency,
    completed,
    age
  ) {
    super(visitId, name, doctor, purpose, description, urgency, completed);
    this.age = age;
  }
}

// create VisitTherapist layout
export class VisitTherapistTemplate {
  constructor(visit) {
    this.visit = visit;
    this.div = null;
  }

  render(element) {
    this.div = document.createElement("div");
    this.div.classList.add(parentVisitCss);
    this.div.classList.add(parentVisitShadowCss);
    this.div.dataset.visitId = this.visit.id;

    let doctor = getDoctor(this.visit.doctor);
    let urgency = getUrgency(this.visit.urgency);
    let completed = getCompleted(this.visit.completed);

    let visitLayout = `
      <div class="visit__btn-control">
        <button class="btn-more">Show more</button>
        <button class="btn-less">Show less</button>
        <button class="btn-edit" data-popup-id="form__visit-edit">Edit</button>
        <button class="btn-delete">Delete</button>
      </div>
      <div class="visit__name">ім'я: ${this.visit.name}</div>
      <div class="visit__doctor">лікар: ${doctor}</div>
      <div class="visit__more-info">
       <div class="visit__completed">
          статус:
          <span class="completed__value">${completed}</span>
        </div>
         <div class="visit__urgency">
         терміновість:
          <span class="urgency__value">${urgency}</span>
        </div>
        <div class="visit__purpose">
        мета візиту:
          <span class="purpose__value">${this.visit.purpose}</span>
        </div>
        <div class="visit__description">
        короткий опис візиту:
          <span class="description__value">${this.visit.description}</span>
        </div>   
        <div class="visit__age">
          вік:
          <span class="age__value">${this.visit.age}</span>
        </div>
          </div>   
    `;

    this.div.insertAdjacentHTML("beforeend", visitLayout);
    element.append(this.div);

    // подія для показу більше інформації/редагувати/видалити
    this.div.addEventListener("click", (e) => visitHandler(e));
  }
}